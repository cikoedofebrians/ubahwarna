import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ubah Warna',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.orange),
        useMaterial3: true,
      ),
      home: const MainScreen(),
    );
  }
}

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final List<Color> colorList = [
    Colors.orange,
    Colors.blue,
    Colors.pink,
  ];

  //
  int colorIndex = 0;
  // function to get the colored box
  Widget getBox() {
    return Container(
      height: 200,
      width: 200,
      color: colorList[colorIndex],
    );
  }

  // function to get the button widget that will trigger the change color function
  Widget getButton() {
    return ElevatedButton(
      onPressed: changeColor,
      style: const ButtonStyle(
        backgroundColor: MaterialStatePropertyAll(Colors.red),
      ),
      child: const Text(
        "Ubah Warna",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  // function that will be triggered by the button
  void changeColor() {
    // Check if the colorIndex is under 2, so that the index won't go offside
    if (colorIndex < 2) {
      setState(() {
        colorIndex += 1; // if true, then add it by 1
      });
    } else {
      setState(() {
        colorIndex = 0; // if false, start it from 0 again
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text("Latihan Flutter"),
        backgroundColor: Colors.yellow,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            getBox(),
            getButton(),
          ],
        ),
      ),
    );
  }
}
